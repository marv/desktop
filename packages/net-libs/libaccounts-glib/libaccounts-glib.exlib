# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=accounts-sso tag=VERSION_${PV} suffix=tar.bz2 ] test-dbus-daemon \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_prepare

SUMMARY="Accounts management library for GLib applications"
DESCRIPTION="
This project is a library for managing accounts which can be used from GLib
applications. It is part of the accounts-sso project.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    python
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools[>=1.14] [[ note = [ for gtkdocize ] ]]
        app-text/docbook-xml-dtd:4.3 [[ note = [ for man pages ] ]]
        app-text/docbook-xsl-stylesheets [[ note = [ for man pages ] ]]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-db/sqlite:3[>=3.7.0]
        dev-libs/glib:2[>=2.35.1]
        dev-libs/libxml2:2.0
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        python? ( gnome-bindings/pygobject:3[>=2.90] )
    test:
        dev-libs/check[>=0.9.4]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-man
    --disable-gcov
    --with-testdir=
    --with-testdatadir=
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
    python
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--any --enable-tests --disable-tests' )

DEFAULT_SRC_INSTALL_PARAMS=(
    backupconfdir=/usr/share/backup-framework/applications
)

libaccounts-glib_src_prepare() {
    # fix filesdir locations in the pkg-config file
    edo sed -e "/filesdir/s:=\${prefix}:=/usr:" \
            -i ${PN}.pc.in

    # automake: error: cannot open < gtk-doc.make: No such file or directory
    edo gtkdocize --copy --flavour no-tmpl

    autotools_src_prepare
}


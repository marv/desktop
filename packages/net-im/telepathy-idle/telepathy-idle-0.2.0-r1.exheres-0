# Copyright 2010 Paul Seidler
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'telepathy-idle-0.2.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require python test-dbus-daemon

SUMMARY="IRC Plugin for Telepathy"
HOMEPAGE="http://telepathy.freedesktop.org/wiki/"
DOWNLOADS="http://telepathy.freedesktop.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.3]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/dbus-glib:1[>=0.51]
        dev-libs/glib:2[>=2.32]
        net-im/telepathy-glib[>=0.21]
        sys-apps/dbus[>=0.51]
    test:
        dev-python/dbus-python
        dev-python/pyopenssl[python_abis:2.7]
        gnome-bindings/pygobject:2[python_abis:2.7]
        net-twisted/Twisted
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-Fix-invalid-utf8-test.patch )

src_prepare() {
    default

    # Fails since 0.1.16 and hasn't been picked up since october 2016,
    # upstream is busy working on 1.0
    edo sed -e 's:connect/server-quit-ignore.py::' \
        -e 's:connect/server-quit-noclose.py::' \
        -i tests/twisted/Makefile.{am,in}
}

src_test() {
    esandbox allow_net "inet:0.0.0.0@6900"
    esandbox allow_net --connect "inet:127.0.0.1@5600"
    esandbox allow_net --connect "inet:127.0.0.1@6900"
    test-dbus-daemon_run-tests
    esanbdox disallow_net --connect "inet:127.0.0.1@6900"
    esandbox disallow_net --connect "inet:127.0.0.1@5600"
    esandbox disallow_net "inet:0.0.0.0@6900"
}

